﻿using Otus.Teaching.Pcf.GivingToCustomer.Cores.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IDistributedCache
    {
        Task<bool> AddPreferencesToCache(Guid redisKey, IEnumerable<PreferenceResponse> preferenceResponses);
        Task<IEnumerable<PreferenceResponse>> GetPreferencesFromCache(Guid redisKey);
    }
}

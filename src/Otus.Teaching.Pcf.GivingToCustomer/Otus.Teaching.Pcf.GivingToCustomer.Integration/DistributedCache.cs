﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
//using System.Text.Json;
using Newtonsoft.Json;
using System.Text;
using Otus.Teaching.Pcf.GivingToCustomer.Cores.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class DistributedCache :
        IDistributedCache
    {
        private readonly HttpClient _httpClient;

        public DistributedCache(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<bool> AddPreferencesToCache(Guid redisKey, IEnumerable<PreferenceResponse> preferenceResponses)
        {
            var response = await _httpClient.PostAsync($"api/v1/Preferences?redisKey={redisKey}", 
                new StringContent(JsonConvert.SerializeObject(preferenceResponses), Encoding.UTF8, "application/json"))
                .Result.Content.ReadAsStringAsync();

            return Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(response);
        }

        public async Task<IEnumerable<PreferenceResponse>> GetPreferencesFromCache(Guid redisKey)
        {
            string stringTask = await _httpClient.GetStringAsync($"api/v1/Preferences?redisKey={redisKey}");

            return  JsonConvert.DeserializeObject<IEnumerable<PreferenceResponse>>(stringTask);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Redis.Models;
using StackExchange.Redis;
using Newtonsoft.Json;


namespace WebHost.Controllers
{
    /// <summary>
    /// Кэш 
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly IDatabase _redis;

        public PreferencesController(IDatabase redis)
        {
            _redis = redis;
        }

        /// <summary>
        /// Закэшировать.
        /// </summary>
        /// <param name="redisKey">Ключ кэша.</param>
        /// <param name="preferenceResponses">Модель для кэширование.</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> AddPreferencesToCache(Guid redisKey, IEnumerable<PreferenceResponse> preferenceResponses)
        {
            if (preferenceResponses != null)
            {
                return await _redis.StringSetAsync(redisKey.ToString(), JsonConvert.SerializeObject(preferenceResponses), flags: CommandFlags.FireAndForget);
            }
            else
                return false;
        }

        /// <summary>
        /// Получить с кэша.
        /// </summary>
        /// <param name="redisKey">Ключ кэша.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceResponse>>GetPreferencesFromCache(Guid redisKey)
        {
            string cache = await _redis.StringGetAsync(redisKey.ToString());

            if (!string.IsNullOrEmpty(cache))
            {
                return JsonConvert.DeserializeObject<IEnumerable<PreferenceResponse>>(cache);
            }
            return null;
        }
    }
}
